module Lexer where

import Control.Applicative (liftA2)

import Text.ParserCombinators.Parsec
import Text.ParserCombinators.Parsec.Language
import qualified Text.ParserCombinators.Parsec.Token as Token

lisfio = emptyDef {
    Token.identStart = letter,
    Token.identLetter = alphaNum,
    Token.reservedNames = [
        "do",
        "if",
        "in",
        "let",
        "var",
        "else",
        "then",
        "skip",
        "fail",
        "with",
        "catch",
        "while"
    ],
    Token.reservedOpNames = [
        "!", "?", ";", ":=",
        "+", "-", "*", "/",
        "<", "<=", ">", "<=",
        "¬", "&&", "||", "=", "!="
    ]
}

lexer = Token.makeTokenParser lisfio

identifier = Token.identifier lexer
reservedOp = Token.reservedOp lexer
reserved   = Token.reserved lexer
parens     = Token.parens lexer
integer    = Token.integer lexer
semi       = Token.semi lexer
whiteSpace = Token.whiteSpace lexer

