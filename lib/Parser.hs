module Parser where

import Lexer
import Command

import Control.Monad (liftM)
import Text.ParserCombinators.Parsec
import Text.ParserCombinators.Parsec.Expr
import Text.ParserCombinators.Parsec.Language

commandParser :: Parser Command
commandParser = whiteSpace >> command

command :: Parser Command
command =   parens command
        <|> seqCommand

seqCommand :: Parser Command
seqCommand = do
    list <- (sepBy1 command' semi)
    return $ if length list == 1
        then head list
        else Seq list

command' :: Parser Command
command' =   ifStmt
         <|> whileStmt
         <|> catchStmt
         <|> cinStmt
         <|> coutStmt
         <|> skipStmt
         <|> failStmt
         <|> letInStmt
         <|> letStmt

ifStmt :: Parser Command
ifStmt = do
    reserved "if"
    b <- boolExp
    reserved "then"
    c0 <- command
    reserved "else"
    c1 <- command
    return $ If b c0 c1

whileStmt :: Parser Command
whileStmt = do
    reserved "while"
    b <- boolExp
    reserved "do"
    c <- command
    return $ While b c

catchStmt :: Parser Command
catchStmt = do
    reserved "catch"
    c0 <- command
    reserved "with"
    c1 <- command
    return $ Catch c0 c1

cinStmt :: Parser Command
cinStmt = do
    reservedOp "?"
    var <- identifier
    return $ Cin var

coutStmt :: Parser Command
coutStmt = do
    reservedOp "!"
    x <- intExp
    return $ Cout x

skipStmt :: Parser Command
skipStmt = reserved "skip" >> return Skip

failStmt :: Parser Command
failStmt = reserved "fail" >> return Fail

letInStmt :: Parser Command
letInStmt = do
    reserved "let"
    var <- identifier
    reservedOp ":="
    x <- intExp
    reserved "in"
    c <- command
    return $ LetIn var x c

letStmt :: Parser Command
letStmt = do
    reserved "var"
    var <- identifier
    reservedOp ":="
    x <- intExp
    return $ Var var x

intExp :: Parser IntExp
intExp = buildExpressionParser intOps intTerm

intTerm =   parens intExp
        <|> liftM IntVar identifier
        <|> liftM IntConst integer

intOps = [
    [ Prefix $ reservedOp "-" >> return (IntUn  Neg) ],
    [ Infix   (reservedOp "/" >> return (IntBin Div)) AssocLeft ],
    [ Infix   (reservedOp "*" >> return (IntBin Mul)) AssocLeft ],
    [ Infix   (reservedOp "+" >> return (IntBin Add)) AssocLeft,
      Infix   (reservedOp "-" >> return (IntBin Sub)) AssocLeft ]
    ]

intCmp = do
    x <- intExp
    op <- cmpOp
    y <- intExp
    return $ IntCmp op x y

cmpOp =   (reservedOp "<"  >> return Lt)
      <|> (reservedOp ">"  >> return Gt)
      <|> (reservedOp "<=" >> return Le)
      <|> (reservedOp ">=" >> return Ge)
      <|> (reservedOp "="  >> return Eq)
      <|> (reservedOp "!=" >> return Ne)

boolExp :: Parser BoolExp
boolExp = buildExpressionParser boolOps boolTerm

boolTerm =   parens boolExp
         <|> (reserved "true"  >> return (BoolConst True))
         <|> (reserved "false" >> return (BoolConst False))
         <|> intCmp

boolOps = [
    [ Prefix $ reservedOp "!"  >> return (BoolUn  Not),
      Prefix $ reservedOp "¬"  >> return (BoolUn  Not) ],
    [ Infix   (reservedOp "&&" >> return (BoolBin And)) AssocLeft,
      Infix   (reservedOp "||" >> return (BoolBin Or )) AssocLeft ]
    ]


