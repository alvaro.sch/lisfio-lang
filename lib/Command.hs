module Command where

type Ident = String

data IntUnOp = Neg
    deriving (Show, Eq)

data IntBinOp = Add | Sub | Mul | Div
    deriving (Show, Eq)

data IntExp = IntConst Integer
    | IntVar Ident
    | IntUn IntUnOp IntExp
    | IntBin IntBinOp IntExp IntExp
    deriving (Show, Eq)

data BoolUnOp = Not
    deriving (Show, Eq)

data BoolBinOp = And | Or
    deriving (Show, Eq)

data IntCmpOp = Lt | Le | Eq | Ne | Gt | Ge
    deriving (Show, Eq)

data BoolExp = BoolConst Bool
    | BoolUn BoolUnOp BoolExp
    | BoolBin BoolBinOp BoolExp BoolExp
    | IntCmp IntCmpOp IntExp IntExp
    deriving (Show, Eq)

data Command = Skip
    | Fail
    | Seq [Command]
    | Var Ident IntExp
    | LetIn Ident IntExp Command
    | If BoolExp Command Command
    | While BoolExp Command
    | Catch Command Command
    | Cout IntExp
    | Cin Ident
    deriving (Show, Eq)

