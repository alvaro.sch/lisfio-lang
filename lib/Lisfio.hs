module Lisfio (
    parseString, parseFile, Ident,
    Command(..), IntUnOp(..), IntBinOp(..), IntExp(..),
    BoolUnOp(..), BoolBinOp(..), IntCmpOp(..), BoolExp(..)
) where

import Lexer
import Command
import Parser

import Text.ParserCombinators.Parsec

parseString :: String -> Command
parseString str =
  case parse commandParser "" str of
    Left e  -> error $ show e
    Right r -> r

parseFile :: String -> IO Command
parseFile file =
  do program  <- readFile file
     case parse commandParser "" program of
       Left e  -> print e >> fail "parse error"
       Right r -> return r

