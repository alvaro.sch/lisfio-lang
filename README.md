# LISFIO

LISFIO is a simple imperative language with failure and IO.

The acronym stands for Looking for Isomorphysms Seemingly Fooling Inside Omega.

## Syntax

```
<var> ::= (alphanumeric strings)

<intconst> ::= 0 | 1 | ...

<boolconst> ::= true | false

<intexp> ::= <intconst>
    | -<intexp>
    | <intexp> ('+' | '-' | '*' | '/') <intexp>

<boolexp> ::= <boolconst>
    | ('¬' | '!') <boolexp>
    | <boolexp> ('&&' | '||') <boolexp>
    | <intexp> ('<' | '<=' | '=' | '!=' | '>' | '>=') <intexp>

<cmd> ::= skip
    | fail
    | var <var> := <intexp>
    | ( <cmd> )
    | <cmd> ; <cmd>
    | let <var> := <intexp> in <cmd>
    | if <boolexp> then <cmd> else <cmd>
    | while <boolexp> do <cmd>
    | catch <cmd> with <cmd>
    | ! <intexp>
    | ? <var>
```

## Running

The recommended way is to install cabal and run
```sh
$ cabal repl
```

Then use `readString` / `readFile` to get the AST.

