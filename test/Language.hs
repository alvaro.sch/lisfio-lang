module Language (tests) where

import Lisfio
import Common
import qualified Distribution.TestSuite as TS

tests :: IO [TS.Test]
tests = return $ makeSuite [
        ("skip", skip),
        ("fail", fail'),
        ("long if", longIf),
        ("catch in", catchIn),
        ("while seq", whileSeq)
    ]

skip = assertEq prog expected
    where
    prog = parseString "skip"
    expected = Skip

fail' = assertEq prog expected
    where
    prog = parseString "fail"
    expected = Fail

longIf = assertEq prog expected
    where
    prog = parseString "if x = y + z * w then (fail) else let a := 3 in fail"
    expected = If
        (IntCmp Eq
            (IntVar "x")
            (IntBin Add
                (IntVar "y")
                (IntBin Mul (IntVar "z") (IntVar "w"))))
        (Fail)
        (LetIn "a" (IntConst 3) Fail)

catchIn = assertEq prog expected
    where
    prog = parseString "catch ?x with if !true then !x else fail"
    expected = Catch
        (Cin "x")
        (If (BoolUn Not (BoolConst True)) (Cout (IntVar "x")) Fail)

whileSeq = assertEq prog expected
    where
    prog = parseString "while true do (skip; skip; skip); skip"
    expected = Seq [
            While
                (BoolConst True)
                (Seq [Skip, Skip, Skip]),
            Skip
        ]

