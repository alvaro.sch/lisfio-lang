module Expressions (tests) where

import Lisfio
import Common
import qualified Distribution.TestSuite as TS

tests :: IO [TS.Test]
tests = return $ makeSuite [
        ("int presedence", intPresedence)
    ]

intPresedence :: IO TS.Progress
intPresedence = assertEq prog expected
    where
    prog = parseString "!2 + 5 / 8 * 1"
    expected = Cout $ IntBin Add
        (IntConst 2)
        (IntBin Mul
            (IntBin Div
                (IntConst 5)
                (IntConst 8)
            )
            (IntConst 1)
        )

