module Common (makeSuite, makeTest, assertEq) where

import Distribution.TestSuite

makeSuite = map (Test . makeTest)

makeTest (name, test) = TestInstance {
    run = test,
    name = name,
    tags = [],
    options = [],
    setOption = \_ _ -> Right $ makeTest (name, test)
}

assertEq l r
    | l == r = return $ Finished Pass
    | otherwise = return $ Finished $ Fail msg
    where msg = writeErr l r

writeErr l r = "assertion failed! "
    ++ "[left: " ++ show l ++ "] "
    ++ "[right: " ++ show r ++ "] "

